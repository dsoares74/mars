package com.nasa.mars.gateway.http;

import com.nasa.mars.converter.PlateauConverter;
import com.nasa.mars.converter.PositionJsonConverter;
import com.nasa.mars.converter.RoverConverter;
import com.nasa.mars.gateway.http.json.ExplorationJson;
import com.nasa.mars.model.Plateau;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;
import com.nasa.mars.usecases.ExploreMars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rover")
public class RoverController {

    private ExploreMars exploreMars;
    private RoverConverter roverConverter;
    private PlateauConverter plateauConverter;
    private PositionJsonConverter positionConverter;

    @Autowired
    public RoverController(final ExploreMars exploreMars,
                           final RoverConverter roverConverter,
                           final PlateauConverter plateauConverter,
                           final PositionJsonConverter positionConverter) {
        this.exploreMars = exploreMars;
        this.roverConverter = roverConverter;
        this.plateauConverter = plateauConverter;
        this.positionConverter = positionConverter;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> explore(@RequestBody @Valid final ExplorationJson json) {
        final Plateau plateau = plateauConverter.convert(json.getPlateau());
        final List<Rover> rovers = roverConverter.convert(json.getRovers());
        final List<Position> positions = exploreMars.startExploration(plateau, rovers);

        return new ResponseEntity(positionConverter.convert(positions), HttpStatus.OK);
    }
}
