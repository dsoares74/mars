package com.nasa.mars.gateway.http.json;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class PlateauJson implements Serializable {

    private static final long serialVersionUID = 3121645376471687304L;

    @Min(1)
    @NotNull
    private Integer width;

    @Min(1)
    @NotNull
    private Integer height;


    public Integer getWidth() {
        return width;
    }

    public void setWidth(final Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(final Integer height) {
        this.height = height;
    }
}
