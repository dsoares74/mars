package com.nasa.mars.gateway.http.json;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class ExplorationJson implements Serializable {

    private static final long serialVersionUID = -1352840640301654328L;

    @Valid
    @NotNull
    private PlateauJson plateau;

    @Valid
    @Size(min = 1)
    private List<RoverJson> rovers;

    public PlateauJson getPlateau() {
        return plateau;
    }

    public void setPlateau(final PlateauJson plateau) {
        this.plateau = plateau;
    }

    public List<RoverJson> getRovers() {
        return rovers;
    }

    public void setRovers(final List<RoverJson> rovers) {
        this.rovers = rovers;
    }
}
