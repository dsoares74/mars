package com.nasa.mars.gateway.http.json;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class RoverJson implements Serializable {

    private static final long serialVersionUID = 7971872907996296572L;

    @Valid
    @NotNull
    private PositionJson position;

    @NotBlank
    private String instructions;

    public PositionJson getPosition() {
        return position;
    }

    public void setPosition(final PositionJson position) {
        this.position = position;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(final String instructions) {
        this.instructions = instructions;
    }
}