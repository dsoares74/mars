package com.nasa.mars.gateway.http.json;

import javax.validation.constraints.*;
import java.io.Serializable;

public class PositionJson implements Serializable {

    private static final long serialVersionUID = -3264617272501236373L;

    @Min(0)
    @NotNull
    private Integer x;

    @Min(0)
    @NotNull
    private Integer y;

    @NotBlank
    @Size(min = 1, max = 1)
    @Pattern(regexp = "^(N|S|E|W)$")
    private String direction;

    public PositionJson() {
        super();
    }

    public PositionJson(final Integer x, final Integer y, final String direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Integer getX() {
        return x;
    }

    public void setX(final Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(final Integer y) {
        this.y = y;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(final String direction) {
        this.direction = direction;
    }
}
