package com.nasa.mars.model;

import com.nasa.mars.exception.InvalidCommandTypeException;

import java.util.Arrays;

public enum CommandType {
    MOVE("M"),
    LEFT("L"),
    RIGHT("R");

    private String code;

    CommandType(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static CommandType findByCode(final String code) {
        return Arrays.stream(values())
                .filter(commandType -> commandType.getCode().equalsIgnoreCase(code)).findFirst()
                .orElseThrow(() -> new InvalidCommandTypeException("Command type is invalid"));
    }
}
