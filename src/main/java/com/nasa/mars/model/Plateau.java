package com.nasa.mars.model;

public class Plateau {
    private final int x;
    private final int y;
    private final int width;
    private final int height;

    public Plateau(final int width, final int height) {
        this(0, 0, width, height);
    }

    public Plateau(final int x, final int y, final int width, final int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public boolean inside(final int xn, final int yn) {
        int w = this.width;
        int h = this.height;

        final int x0 = this.x;
        final int y0 = this.y;
        if (xn < x0 || yn < y0) {
            return false;
        }

        w += x0;
        h += y0;
        //    overflow || intersect
        return ((w < x0 || w >= xn) &&
                (h < y0 || h >= yn));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String toString() {
        return "Plateau[x=" + x + ",y=" + y + ",width=" + width + ",height=" + height + "]";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Plateau)) {
            return false;
        }

        final Plateau plateau = (Plateau) o;

        if (getX() != plateau.getX()) {
            return false;
        }
        if (getY() != plateau.getY()) {
            return false;
        }
        if (getWidth() != plateau.getWidth()) {
            return false;
        }
        return getHeight() == plateau.getHeight();
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        result = 31 * result + getWidth();
        result = 31 * result + getHeight();
        return result;
    }
}
