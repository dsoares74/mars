package com.nasa.mars.model;

public class Position {
    private int x;
    private int y;
    private Direction direction;

    public Position() {
        this(0,0, Direction.NORTH);
    }

    public Position(final int x, final int y, final Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Position(final Position position) {
        this(position.getX(), position.getY(), position.getDirection());
    }

    public void move(final int x1, final int y1) {
        this.x += x1;
        this.y += y1;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Position[x: " + x + ", y: " + y +", direction: " + direction +"]";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Position)) {
            return false;
        }

        final Position position = (Position) o;

        if (getX() != position.getX()) {
            return false;
        }
        if (getY() != position.getY()) {
            return false;
        }
        return getDirection() == position.getDirection();
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        result = 31 * result + getDirection().hashCode();
        return result;
    }
}
