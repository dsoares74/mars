package com.nasa.mars.model;

import com.nasa.mars.exception.InvalidDirectionException;

import java.util.function.Predicate;

import static java.util.Arrays.stream;

public enum Direction {
    NORTH(90, "N"),
    SOUTH(270, "S"),
    EAST(180, "E"),
    WEST(0, "W");

    private int angle;
    private String code;

    Direction(final int angle, final String code) {
        this.code = code;
        this.angle = angle;
    }

    public int getAngle() {
        return angle;
    }

    public String getCode() {
        return code;
    }

    public static Direction findByCode(final String code) {
        return findByPredicate(direction -> direction.getCode().equalsIgnoreCase(code));
    }

    public static Direction findByAngle(final int angle) {
        return findByPredicate(direction -> direction.getAngle() == angle);
    }

    private static Direction findByPredicate(final Predicate<Direction> predicate) {
        return stream(values()).filter(predicate).findFirst()
                .orElseThrow(() -> new InvalidDirectionException("Direction is invalid"));
    }
}
