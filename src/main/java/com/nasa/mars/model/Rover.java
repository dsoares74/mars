package com.nasa.mars.model;

public class Rover {
    private Position position;
    private String instructions;

    public Rover(final Position position, final String instructions) {
        this.position = position;
        this.instructions = instructions;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(final Position position) {
        this.position = position;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(final String instructions) {
        this.instructions = instructions;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rover)) {
            return false;
        }

        final Rover rover = (Rover) o;

        if (getPosition() != null ? !getPosition().equals(rover.getPosition()) : rover.getPosition() != null) {
            return false;
        }
        return getInstructions() != null ? getInstructions().equals(rover.getInstructions()) : rover.getInstructions() == null;
    }

    @Override
    public int hashCode() {
        int result = getPosition() != null ? getPosition().hashCode() : 0;
        result = 31 * result + (getInstructions() != null ? getInstructions().hashCode() : 0);
        return result;
    }
}
