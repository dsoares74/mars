package com.nasa.mars.usecases;

import com.nasa.mars.model.CommandType;
import com.nasa.mars.model.Position;
import org.springframework.stereotype.Component;

@Component
public class MoveRover implements Command {

    private static final int STEP = 1;

    @Override
    public Position execute(final Position position) {
        final Position p = new Position(position);

        switch (position.getDirection()) {
            case NORTH:
                p.move(0, STEP);
                break;
            case SOUTH:
                p.move(0, -STEP);
                break;
            case EAST:
                p.move(STEP, 0);
                break;
            case WEST:
                p.move(-STEP, 0);
                break;
        }

        return p;
    }

    @Override
    public CommandType getType() {
        return CommandType.MOVE;
    }
}
