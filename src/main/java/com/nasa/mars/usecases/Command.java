package com.nasa.mars.usecases;

import com.nasa.mars.model.CommandType;
import com.nasa.mars.model.Position;

public interface Command {

    Position execute(Position position);

    CommandType getType();
}
