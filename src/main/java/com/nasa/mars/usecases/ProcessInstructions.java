package com.nasa.mars.usecases;

import com.nasa.mars.exception.InvalidInstructionException;
import com.nasa.mars.model.CommandType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ProcessInstructions {

    private static final String COMMAND_PATTERN = "LMR";

    private Map<CommandType, Command> commandMap;

    @Autowired
    public ProcessInstructions(final List<Command> commands) {
        this.commandMap = new EnumMap<>(CommandType.class);
        commands.forEach(command -> commandMap.put(command.getType(), command));
    }

    public List<Command> execute(final String instructions) {
        validateCommands(instructions);
        return convertToCommands(instructions);
    }

    private List<Command> convertToCommands(final String instructions) {
        return instructions.toUpperCase().codePoints().mapToObj(c -> String.valueOf((char) c))
                .map(CommandType::findByCode).map(commandMap::get).collect(Collectors.toList());
    }

    private void validateCommands(final String commands) {
        if (StringUtils.isBlank(commands) || !StringUtils.containsOnly(commands.toUpperCase(), COMMAND_PATTERN)) {
            throw new InvalidInstructionException("One or more instructions are invalid");
        }
    }
}
