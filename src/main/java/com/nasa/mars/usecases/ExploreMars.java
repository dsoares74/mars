package com.nasa.mars.usecases;

import com.nasa.mars.exception.InvalidPositionException;
import com.nasa.mars.model.Plateau;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
public class ExploreMars {
    private static final Logger logger = Logger.getLogger(ExploreMars.class.getCanonicalName());

    private ProcessInstructions processor;

    @Autowired
    public ExploreMars(final ProcessInstructions processor) {
        this.processor = processor;
    }

    public List<Position> startExploration(final Plateau plateau, final List<Rover> rovers) {
        return rovers.stream().peek(rover -> executeInstructions(plateau, rover))
                .map(Rover::getPosition).collect(Collectors.toList());
    }

    private void executeInstructions(final Plateau plateau, final Rover rover) {
        final List<Command> commands = processor.execute(rover.getInstructions());

        commands.forEach(command -> {
            final Position position = command.execute(rover.getPosition());
            logger.info(String.format("Rover moved to position: '%s' ", position));

            this.validatePosition(plateau, position);
            rover.setPosition(position);
        });
    }

    private void validatePosition(final Plateau plateau, final Position position) {
        if (!plateau.inside(position.getX(), position.getY())) {
            throw new InvalidPositionException("Invalid position to this plateau");
        }
    }

}
