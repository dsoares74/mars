package com.nasa.mars.usecases;

import com.nasa.mars.model.CommandType;
import com.nasa.mars.model.Direction;
import com.nasa.mars.model.Position;
import org.springframework.stereotype.Component;

@Component
public class TurnRightRover implements Command {

    private static final int RIGHT_ANGLE = 90;

    @Override
    public Position execute(final Position position) {
        final Direction direction = (position.getDirection() == Direction.SOUTH ?
                Direction.WEST : Direction.findByAngle(position.getDirection().getAngle() + RIGHT_ANGLE));

        return new Position(position.getX(), position.getY(), direction);
    }

    @Override
    public CommandType getType() {
        return CommandType.RIGHT;
    }
}
