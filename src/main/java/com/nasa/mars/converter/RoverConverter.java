package com.nasa.mars.converter;

import com.nasa.mars.gateway.http.json.RoverJson;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoverConverter implements Converter<List<RoverJson>, List<Rover>> {

    private PositionConverter positionConverter;

    @Autowired
    public RoverConverter(final PositionConverter positionConverter) {
        this.positionConverter = positionConverter;
    }

    private Rover convert(final RoverJson roverJson) {
        final Position position = positionConverter.convert(roverJson.getPosition());
        return new Rover(position, roverJson.getInstructions());
    }

    @Nullable
    @Override
    public List<Rover> convert(final List<RoverJson> roverJsons) {
        return roverJsons.stream().map(this::convert).collect(Collectors.toList());
    }
}
