package com.nasa.mars.converter;

import com.nasa.mars.gateway.http.json.PositionJson;
import com.nasa.mars.model.Direction;
import com.nasa.mars.model.Position;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class PositionConverter implements Converter<PositionJson, Position> {

    @Nullable
    @Override
    public Position convert(final PositionJson positionJson) {
        return new Position(positionJson.getX(), positionJson.getY(), Direction.findByCode(positionJson.getDirection()));
    }
}
