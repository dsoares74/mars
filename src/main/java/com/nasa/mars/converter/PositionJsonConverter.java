package com.nasa.mars.converter;

import com.nasa.mars.gateway.http.json.PositionJson;
import com.nasa.mars.model.Position;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PositionJsonConverter implements Converter<List<Position>, List<PositionJson>> {

    private PositionJson convert(final Position position) {
        return new PositionJson(position.getX(), position.getY(), position.getDirection().getCode());
    }

    @Nullable
    @Override
    public List<PositionJson> convert(final List<Position> positions) {
        return positions.stream().map(this::convert).collect(Collectors.toList());
    }
}
