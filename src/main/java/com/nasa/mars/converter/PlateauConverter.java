package com.nasa.mars.converter;

import com.nasa.mars.gateway.http.json.PlateauJson;
import com.nasa.mars.model.Plateau;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class PlateauConverter implements Converter<PlateauJson, Plateau> {

    @Nullable
    @Override
    public Plateau convert(final PlateauJson plateauJson) {
        return new Plateau(plateauJson.getWidth(), plateauJson.getHeight());
    }
}
