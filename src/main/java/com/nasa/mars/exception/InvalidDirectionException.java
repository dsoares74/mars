package com.nasa.mars.exception;

public class InvalidDirectionException extends RuntimeException{

    private static final long serialVersionUID = 2881096768101291940L;

    public InvalidDirectionException() {
        super();
    }

    public InvalidDirectionException(final String message) {
        super(message);
    }
}
