package com.nasa.mars.exception;

public class InvalidInstructionException extends RuntimeException {
    private static final long serialVersionUID = -3443091503428198728L;

    public InvalidInstructionException() {
        super();
    }

    public InvalidInstructionException(final String message) {
        super(message);
    }
}
