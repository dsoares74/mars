package com.nasa.mars.exception;

public class InvalidPositionException extends RuntimeException {
    private static final long serialVersionUID = 42065872532190806L;

    public InvalidPositionException() {
        super();
    }

    public InvalidPositionException(final String message) {
        super(message);
    }
}
