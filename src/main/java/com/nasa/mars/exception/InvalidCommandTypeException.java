package com.nasa.mars.exception;

public class InvalidCommandTypeException extends RuntimeException {
    private static final long serialVersionUID = 447057943189088455L;

    public InvalidCommandTypeException() {
        super();
    }

    public InvalidCommandTypeException(final String message) {
        super(message);
    }
}
