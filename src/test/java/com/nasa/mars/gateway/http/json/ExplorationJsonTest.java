package com.nasa.mars.gateway.http.json;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import static com.nasa.mars.templates.ExplorationJsonTemplate.VALID;
import static com.nasa.mars.templates.RoverJsonTemplate.LMLMLMLMM;
import static com.nasa.mars.templates.RoverJsonTemplate.MMRMMRMRRM;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest
public class ExplorationJsonTest {

    @Autowired
    private JacksonTester<ExplorationJson> json;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
    }

    @Test
    public void testSerialize() throws Exception {
        final ExplorationJson exploration = Fixture.from(ExplorationJson.class).gimme(VALID);
        assertThat(this.json.write(exploration)).hasJsonPathValue("@.plateau");
        assertThat(this.json.write(exploration)).hasJsonPathArrayValue("@.rovers");
        assertThat(this.json.write(exploration))
                .extractingJsonPathNumberValue("@.plateau.width").isEqualTo(5);
        assertThat(this.json.write(exploration))
                .extractingJsonPathNumberValue("@.plateau.height").isEqualTo(5);
        assertThat(this.json.write(exploration))
                .extractingJsonPathArrayValue("@.rovers[*].position.x").containsExactly(1, 3);
        assertThat(this.json.write(exploration))
                .extractingJsonPathArrayValue("@.rovers[*].position.y").containsExactly(2, 3);
        assertThat(this.json.write(exploration))
                .extractingJsonPathArrayValue("@.rovers[*].position.direction").containsExactly("N", "E");
        assertThat(this.json.write(exploration))
                .extractingJsonPathArrayValue("@.rovers[*].instructions").containsExactly(LMLMLMLMM, MMRMMRMRRM);
    }

    @Test
    public void testDeserialize() throws Exception {
        final String content = "{\"plateau\":{\"width\":7,\"height\":6},\"rovers\":[{\"position\":{\"x\":2,\"y\":4,\"direction\":\"W\"},\"instructions\":\"LMRMM\"}]}";
        assertThat(this.json.parseObject(content).getPlateau()).isNotNull();
        assertThat(this.json.parseObject(content).getRovers()).isNotEmpty();
        assertThat(this.json.parseObject(content).getPlateau().getWidth()).isEqualTo(7);
        assertThat(this.json.parseObject(content).getPlateau().getHeight()).isEqualTo(6);
        assertThat(this.json.parseObject(content).getRovers().get(0)).isNotNull();
        assertThat(this.json.parseObject(content).getRovers().get(0).getPosition()).isNotNull();
        assertThat(this.json.parseObject(content).getRovers().get(0).getInstructions()).isNotEmpty();
        assertThat(this.json.parseObject(content).getRovers().get(0).getInstructions()).isEqualTo("LMRMM");
        assertThat(this.json.parseObject(content).getRovers().get(0).getPosition().getX()).isEqualTo(2);
        assertThat(this.json.parseObject(content).getRovers().get(0).getPosition().getY()).isEqualTo(4);
        assertThat(this.json.parseObject(content).getRovers().get(0).getPosition().getDirection()).isEqualTo("W");
    }
}