package com.nasa.mars.gateway.http;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.nasa.mars.converter.PlateauConverter;
import com.nasa.mars.converter.PositionJsonConverter;
import com.nasa.mars.converter.RoverConverter;
import com.nasa.mars.model.Plateau;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;
import com.nasa.mars.templates.PlateauTemplate;
import com.nasa.mars.usecases.ExploreMars;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static com.nasa.mars.templates.PositionTemplate.VALID_1_X_3_N;
import static com.nasa.mars.templates.RoverTemplate.VALID_1;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RoverController.class)
public class RoverControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExploreMars exploreMars;

    @Autowired
    private RoverConverter roverConverter;

    @Autowired
    private PlateauConverter plateauConverter;

    @Autowired
    private PositionJsonConverter positionConverter;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
    }

    @Test
    public void testExploreSuccess() throws Exception {
        final List<Rover> rovers = Fixture.from(Rover.class).gimme(1, VALID_1);
        final Plateau plateau = Fixture.from(Plateau.class).gimme(PlateauTemplate.VALID);
        final List<Position> positions = Fixture.from(Position.class).gimme(1, VALID_1_X_3_N);

        given(exploreMars.startExploration(plateau, rovers)).willReturn(positions);

        this.mvc.perform(MockMvcRequestBuilders.post("/rover")
                .content("{\"plateau\":{\"width\":5,\"height\":5},\"rovers\":[{\"position\":{\"x\":1,\"y\":2,\"direction\":\"N\"},\"instructions\":\"LMLMLMLMM\"}]}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].x", is(positions.get(0).getX())))
                .andExpect(jsonPath("$[0].y", is(positions.get(0).getY())))
                .andExpect(jsonPath("$[0].direction", is(positions.get(0).getDirection().getCode())));
    }

}