package com.nasa.mars.converter;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.nasa.mars.gateway.http.json.PlateauJson;
import com.nasa.mars.model.Plateau;
import org.junit.Before;
import org.junit.Test;

import static com.nasa.mars.templates.PlateauJsonTemplate.VALID;
import static org.assertj.core.api.Assertions.assertThat;

public class PlateauConverterTest {

    private PlateauConverter converter;

    @Before
    public void setUp() throws Exception {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
        converter = new PlateauConverter();
    }

    @Test
    public void convertSuccess() throws Exception {
        final PlateauJson json = Fixture.from(PlateauJson.class).gimme(VALID);
        final Plateau plateau = converter.convert(json);
        assertThat(plateau).isNotNull();
        assertThat(json.getWidth()).isEqualTo(plateau.getWidth());
        assertThat(json.getHeight()).isEqualTo(plateau.getHeight());
    }

}