package com.nasa.mars.converter;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.nasa.mars.gateway.http.json.PositionJson;
import com.nasa.mars.model.Position;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.nasa.mars.templates.PositionTemplate.VALID_1_X_2_N;
import static org.junit.Assert.assertTrue;

public class PositionJsonConverterTest {
    private PositionJsonConverter converter;

    @Before
    public void setUp() throws Exception {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
        converter = new PositionJsonConverter();
    }

    @Test
    public void convert() throws Exception {
        final List<Position> positions = Fixture.from(Position.class).gimme(1, VALID_1_X_2_N);
        final List<PositionJson> positionJsons = converter.convert(positions);

        assertTrue(positionJsons.stream().allMatch(json -> json.getX().equals(positions.get(0).getX())
                && json.getY().equals(positions.get(0).getY())
                && json.getDirection().equals(positions.get(0).getDirection().getCode())));
    }

}