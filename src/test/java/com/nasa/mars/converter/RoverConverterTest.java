package com.nasa.mars.converter;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.nasa.mars.gateway.http.json.RoverJson;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.nasa.mars.templates.PositionTemplate.VALID_1_X_2_N;
import static com.nasa.mars.templates.RoverJsonTemplate.VALID_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoverConverterTest {

    @Autowired
    private RoverConverter converter;

    @MockBean
    private PositionConverter positionConverter;

    @Before
    public void setUp() throws Exception {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
    }

    @Test
    public void convertSuccess() throws Exception {
        final List<RoverJson> list = Fixture.from(RoverJson.class).gimme(1, VALID_1);
        final Position position = Fixture.from(Position.class).gimme(VALID_1_X_2_N);
        given(positionConverter.convert(list.get(0).getPosition())).willReturn(position);

        final List<Rover> rovers = converter.convert(list);

        assertThat(rovers).isNotNull();
        assertFalse(rovers.isEmpty());
        assertThat(rovers.get(0).getPosition().getX()).isEqualTo(list.get(0).getPosition().getX());
        assertThat(rovers.get(0).getPosition().getY()).isEqualTo(list.get(0).getPosition().getY());
        assertThat(rovers.get(0).getPosition().getDirection().getCode()).isEqualTo(list.get(0).getPosition().getDirection());
        assertThat(rovers.get(0).getInstructions()).isEqualTo(list.get(0).getInstructions());
    }

}