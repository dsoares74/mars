package com.nasa.mars.converter;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.nasa.mars.exception.InvalidDirectionException;
import com.nasa.mars.gateway.http.json.PositionJson;
import com.nasa.mars.model.Position;
import org.junit.Before;
import org.junit.Test;

import static com.nasa.mars.templates.PositionJsonTemplate.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PositionConverterTest {
    private PositionConverter converter;

    @Before
    public void setUp() throws Exception {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
        converter = new PositionConverter();
    }

    @Test
    public void convertPositionInNorth() throws Exception {
        final PositionJson json = Fixture.from(PositionJson.class).gimme(VALID_1_X_2_N);
        checkConvertion(json);
    }

    @Test
    public void convertPositionInSouth() throws Exception {
        final PositionJson json = Fixture.from(PositionJson.class).gimme(VALID_5_X_1_S);
        checkConvertion(json);
    }

    @Test
    public void convertPositionInWest() throws Exception {
        final PositionJson json = Fixture.from(PositionJson.class).gimme(VALID_1_X_3_W);
        checkConvertion(json);
    }

    @Test
    public void convertPositionInEast() throws Exception {
        final PositionJson json = Fixture.from(PositionJson.class).gimme(VALID_3_X_3_E);
        checkConvertion(json);
    }

    @Test(expected = InvalidDirectionException.class)
    public void convertPositionWithInvalidDirection() throws Exception {
        final PositionJson json = Fixture.from(PositionJson.class).gimme(INVALID_DIRECTION);
        converter.convert(json);
    }

    private void checkConvertion(final PositionJson json) {
        final Position position = converter.convert(json);
        assertThat(position).isNotNull();
        assertThat(json.getX()).isEqualTo(position.getX());
        assertThat(json.getY()).isEqualTo(position.getY());
        assertThat(json.getDirection()).isEqualTo(position.getDirection().getCode());
    }

}