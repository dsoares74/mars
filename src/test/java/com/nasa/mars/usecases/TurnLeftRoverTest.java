package com.nasa.mars.usecases;

import com.nasa.mars.model.CommandType;
import com.nasa.mars.model.Direction;
import com.nasa.mars.model.Position;
import org.junit.Before;
import org.junit.Test;

import static com.nasa.mars.model.Direction.*;
import static org.junit.Assert.assertEquals;

public class TurnLeftRoverTest {
    private TurnLeftRover leftRover;

    @Before
    public void setUp() throws Exception {
        leftRover = new TurnLeftRover();
    }

    @Test
    public void testTurnLeftFromNorth() throws Exception {
        final Position p0 = new Position();
        final Position p1 = leftRover.execute(p0);
        assertEquals(p0.getX(), p1.getX());
        assertEquals(p0.getY(), p1.getY());
        assertEquals(Direction.NORTH, p0.getDirection());
        assertEquals(Direction.WEST, p1.getDirection());
    }

    @Test
    public void testTurnLeftFromSouth() throws Exception {
        final Position p0 = new Position(1,1, SOUTH);
        final Position p1 = leftRover.execute(p0);
        assertEquals(p0.getX(), p1.getX());
        assertEquals(p0.getY(), p1.getY());
        assertEquals(Direction.SOUTH, p0.getDirection());
        assertEquals(Direction.EAST, p1.getDirection());
    }

    @Test
    public void testTurnLeftFromEast() throws Exception {
        final Position p0 = new Position(3,4, EAST);
        final Position p1 = leftRover.execute(p0);
        assertEquals(p0.getX(), p1.getX());
        assertEquals(p0.getY(), p1.getY());
        assertEquals(Direction.EAST, p0.getDirection());
        assertEquals(Direction.NORTH, p1.getDirection());
    }

    @Test
    public void testTurnLeftFromWest() throws Exception {
        final Position p0 = new Position(2,2, WEST);
        final Position p1 = leftRover.execute(p0);
        assertEquals(p0.getX(), p1.getX());
        assertEquals(p0.getY(), p1.getY());
        assertEquals(Direction.WEST, p0.getDirection());
        assertEquals(Direction.SOUTH, p1.getDirection());
    }

    @Test
    public void testGetCommandType() throws Exception {
        assertEquals(CommandType.LEFT, leftRover.getType());
    }

}