package com.nasa.mars.usecases;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.nasa.mars.exception.InvalidPositionException;
import com.nasa.mars.model.Plateau;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.nasa.mars.templates.PlateauTemplate.VALID;
import static com.nasa.mars.templates.PositionTemplate.VALID_1_X_3_N;
import static com.nasa.mars.templates.PositionTemplate.VALID_5_X_1_E;
import static com.nasa.mars.templates.RoverTemplate.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExploreMarsTest {

    @Autowired
    private ExploreMars exploreMars;

    @Autowired
    private ProcessInstructions processInstructions;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates("com.nasa.mars");
    }

    @Test
    public void startExplorationSuccess() throws Exception {
        final List<Rover> rovers = Fixture.from(Rover.class).gimme(2, VALID_1, VALID_2);
        final Plateau plateau = Fixture.from(Plateau.class).gimme(VALID);
        final Position p1 = Fixture.from(Position.class).gimme(VALID_1_X_3_N);
        final Position p2 = Fixture.from(Position.class).gimme(VALID_5_X_1_E);

        final List<Position> positions = exploreMars.startExploration(plateau, rovers);
        assertNotNull(positions);
        assertFalse(positions.isEmpty());
        assertThat(positions.get(0)).isEqualTo(p1);
        assertThat(positions.get(1)).isEqualTo(p2);
    }

    @Test(expected = InvalidPositionException.class)
    public void startExplorationFail() throws Exception {
        final List<Rover> rovers = Fixture.from(Rover.class).gimme(1, INVALID_POSITION);
        final Plateau plateau = Fixture.from(Plateau.class).gimme(VALID);

        exploreMars.startExploration(plateau, rovers);
    }

}