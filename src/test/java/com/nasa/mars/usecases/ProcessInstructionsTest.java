package com.nasa.mars.usecases;

import com.nasa.mars.exception.InvalidInstructionException;
import com.nasa.mars.model.CommandType;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProcessInstructionsTest {

    private ProcessInstructions processInstructions;

    @Before
    public void setUp() throws Exception {
        final List<Command> list = Arrays.asList(new MoveRover(), new TurnLeftRover(), new TurnRightRover());
        processInstructions = new ProcessInstructions(list);
    }

    @Test
    public void executeInstructionsWithoutLeft() throws Exception {
        final String instructions = "MMRMMRMRRM";
        final List<Command> commandList = processInstructions.execute(instructions);
        assertEquals(instructions.length(), commandList.size());
        assertEquals(StringUtils.countMatches(instructions, "M"),
                commandList.stream().filter(command -> command.getType().equals(CommandType.MOVE)).count());
        assertEquals(StringUtils.countMatches(instructions, "R"),
                commandList.stream().filter(command -> command.getType().equals(CommandType.RIGHT)).count());
        assertTrue(commandList.stream().noneMatch(command -> command.getType().equals(CommandType.LEFT)));
    }

    @Test
    public void executeInstructionsWithoutRight() throws Exception {
        final String instructions = "LMLMLMLmm";
        final List<Command> commandList = processInstructions.execute(instructions);
        assertEquals(instructions.length(), commandList.size());
        assertEquals(StringUtils.countMatches(instructions.toUpperCase(), "M"),
                commandList.stream().filter(command -> command.getType().equals(CommandType.MOVE)).count());
        assertEquals(StringUtils.countMatches(instructions, "L"),
                commandList.stream().filter(command -> command.getType().equals(CommandType.LEFT)).count());
        assertTrue(commandList.stream().noneMatch(command -> command.getType().equals(CommandType.RIGHT)));
    }

    @Test(expected = InvalidInstructionException.class)
    public void executeInvalidInstruction() {
        final String instructions = "LMLZX";
        processInstructions.execute(instructions);
    }

    @Test(expected = InvalidInstructionException.class)
    public void executeBlankInstruction() {
        final String instructions = " ";
        processInstructions.execute(instructions);
    }

}