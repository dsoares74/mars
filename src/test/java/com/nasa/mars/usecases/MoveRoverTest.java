package com.nasa.mars.usecases;

import com.nasa.mars.model.CommandType;
import com.nasa.mars.model.Position;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static com.nasa.mars.model.Direction.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class MoveRoverTest {
    private MoveRover moveRover;

    @Before
    public void setUp() throws Exception {
        moveRover = new MoveRover();
    }

    @Test
    public void testMoveToNorth() throws Exception {
        final Position p0 = new Position();
        final Position p1 = moveRover.execute(p0);
        assertEquals(p0.getDirection(), p1.getDirection());
        assertEquals(p0.getX(), p1.getX());
        assertThat(p0.getY(), Matchers.lessThan(p1.getY()));
    }

    @Test
    public void testMoveToSouth() throws Exception {
        final Position p0 = new Position(1,1, SOUTH);
        final Position p1 = moveRover.execute(p0);
        assertEquals(p0.getDirection(), p1.getDirection());
        assertEquals(p0.getX(), p1.getX());
        assertThat(p0.getY(), Matchers.greaterThan(p1.getY()));
    }

    @Test
    public void testMoveToEast() throws Exception {
        final Position p0 = new Position(3,4, EAST);
        final Position p1 = moveRover.execute(p0);
        assertEquals(p0.getDirection(), p1.getDirection());
        assertEquals(p0.getY(), p1.getY());
        assertThat(p0.getX(), Matchers.lessThan(p1.getX()));
    }

    @Test
    public void testMoveToWest() throws Exception {
        final Position p0 = new Position(2,2, WEST);
        final Position p1 = moveRover.execute(p0);
        assertEquals(p0.getDirection(), p1.getDirection());
        assertEquals(p0.getY(), p1.getY());
        assertThat(p0.getX(), Matchers.greaterThan(p1.getX()));
    }

    @Test
    public void testGetCommandType() throws Exception {
        assertEquals(CommandType.MOVE, moveRover.getType());
    }

}