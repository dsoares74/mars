package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.model.Position;
import com.nasa.mars.model.Rover;

public class RoverTemplate implements TemplateLoader {

    private static final String LMLMLMLMM = "LMLMLMLMM";
    private static final String MMRMMRMRRM = "MMRMMRMRRM";

    public static final String VALID_1 = "valid1";
    public static final String VALID_2 = "valid2";
    public static final String INVALID_POSITION = "invalidPosition";

    @Override
    public void load() {
        Fixture.of(Rover.class).addTemplate(VALID_1, new Rule(){{
            add("position", one(Position.class, "1x2N"));
            add("instructions", LMLMLMLMM);
        }});

        Fixture.of(Rover.class).addTemplate(VALID_2, new Rule(){{
            add("position", one(Position.class, "3x3E"));
            add("instructions", MMRMMRMRRM);
        }});

        Fixture.of(Rover.class).addTemplate(INVALID_POSITION, new Rule(){{
            add("position", one(Position.class, "5x1E"));
            add("instructions", MMRMMRMRRM);
        }});

    }
}
