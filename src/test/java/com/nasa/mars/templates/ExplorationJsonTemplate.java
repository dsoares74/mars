package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.gateway.http.json.ExplorationJson;
import com.nasa.mars.gateway.http.json.PlateauJson;
import com.nasa.mars.gateway.http.json.RoverJson;

public class ExplorationJsonTemplate implements TemplateLoader {

    public static final String VALID = "valid";

    @Override
    public void load() {
        Fixture.of(ExplorationJson.class).addTemplate(VALID, new Rule(){{
            add("plateau", one(PlateauJson.class, "valid"));
            add("rovers", has(2).of(RoverJson.class, "valid1", "valid2"));
        }});

    }
}
