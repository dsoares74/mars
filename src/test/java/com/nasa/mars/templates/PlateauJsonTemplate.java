package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.gateway.http.json.PlateauJson;

public class PlateauJsonTemplate implements TemplateLoader {

    public static final String VALID = "valid";

    @Override
    public void load() {
        Fixture.of(PlateauJson.class).addTemplate(VALID, new Rule(){{
            add("width", 5);
            add("height", 5);
        }});

    }
}
