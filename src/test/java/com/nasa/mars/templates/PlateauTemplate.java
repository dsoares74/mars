package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.model.Plateau;

public class PlateauTemplate implements TemplateLoader {

    public static final String VALID = "VALID_1";

    @Override
    public void load() {
        Fixture.of(Plateau.class).addTemplate(VALID, new Rule(){{
            add("x", 0);
            add("y", 0);
            add("width", 5);
            add("height", 5);
        }});

    }
}
