package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.gateway.http.json.PositionJson;
import com.nasa.mars.model.Direction;

public class PositionJsonTemplate implements TemplateLoader {

    public static final String VALID_1_X_2_N = "1x2N";
    public static final String VALID_1_X_3_W = "1x3W";
    public static final String VALID_3_X_3_E = "3x3E";
    public static final String VALID_5_X_1_S = "5x1S";
    public static final String INVALID_DIRECTION = "invalidDirection";

    @Override
    public void load() {
        Fixture.of(PositionJson.class).addTemplate(VALID_1_X_2_N, new Rule(){{
            add("x", 1);
            add("y", 2);
            add("direction", Direction.NORTH.getCode());
        }});

        Fixture.of(PositionJson.class).addTemplate(VALID_1_X_3_W, new Rule(){{
            add("x", 1);
            add("y", 3);
            add("direction", Direction.WEST.getCode());
        }});

        Fixture.of(PositionJson.class).addTemplate(VALID_3_X_3_E, new Rule(){{
            add("x", 3);
            add("y", 3);
            add("direction", Direction.EAST.getCode());
        }});

        Fixture.of(PositionJson.class).addTemplate(VALID_5_X_1_S, new Rule(){{
            add("x", 5);
            add("y", 1);
            add("direction", Direction.SOUTH.getCode());
        }});

        Fixture.of(PositionJson.class).addTemplate(INVALID_DIRECTION, new Rule(){{
            add("x", 4);
            add("y", 2);
            add("direction", "T");
        }});

    }
}
