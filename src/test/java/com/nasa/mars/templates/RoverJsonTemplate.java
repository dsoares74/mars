package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.gateway.http.json.PositionJson;
import com.nasa.mars.gateway.http.json.RoverJson;

import static com.nasa.mars.templates.PositionJsonTemplate.VALID_1_X_2_N;
import static com.nasa.mars.templates.PositionJsonTemplate.VALID_3_X_3_E;

public class RoverJsonTemplate implements TemplateLoader {

    public static final String LMLMLMLMM = "LMLMLMLMM";
    public static final String MMRMMRMRRM = "MMRMMRMRRM";

    public static final String VALID_1 = "valid1";
    public static final String VALID_2 = "valid2";

    @Override
    public void load() {
        Fixture.of(RoverJson.class).addTemplate(VALID_1, new Rule(){{
            add("position", one(PositionJson.class, VALID_1_X_2_N));
            add("instructions", LMLMLMLMM);
        }});

        Fixture.of(RoverJson.class).addTemplate(VALID_2, new Rule(){{
            add("position", one(PositionJson.class, VALID_3_X_3_E));
            add("instructions", MMRMMRMRRM);
        }});

    }
}
