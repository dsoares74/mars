package com.nasa.mars.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.nasa.mars.model.Direction;
import com.nasa.mars.model.Position;

public class PositionTemplate implements TemplateLoader {

    public static final String VALID_1_X_2_N = "1x2N";
    public static final String VALID_1_X_3_N = "1x3N";
    public static final String VALID_3_X_3_E = "3x3E";
    public static final String VALID_5_X_1_E = "5x1E";

    @Override
    public void load() {
        Fixture.of(Position.class).addTemplate(VALID_1_X_2_N, new Rule(){{
            add("x", 1);
            add("y", 2);
            add("direction", Direction.NORTH);
        }});

        Fixture.of(Position.class).addTemplate(VALID_1_X_3_N, new Rule(){{
            add("x", 1);
            add("y", 3);
            add("direction", Direction.NORTH);
        }});

        Fixture.of(Position.class).addTemplate(VALID_3_X_3_E, new Rule(){{
            add("x", 3);
            add("y", 3);
            add("direction", Direction.EAST);
        }});

        Fixture.of(Position.class).addTemplate(VALID_5_X_1_E, new Rule(){{
            add("x", 5);
            add("y", 1);
            add("direction", Direction.EAST);
        }});

    }
}
