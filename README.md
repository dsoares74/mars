# MARS ROVERS

A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.

This project was made using the following technologies:

* Java 8
* Spring Boot 2.0
* Maven
* JUnit
* REST

Build & Run

This project use Maven for build and dependency management.
#####Build

````
$ mvn clean package
````

### Testing
This application contains Integration and Unit Tests
To run, execute the following steps in terminal:

```sh
$ mvn test
```

To start the application, you can run the following command:

`mvn clean spring-boot:run`

Resources

### input

````
{
	"plateau": {
		"width":5,
		"height":5
	},
	"rovers": [
		{
			"position": {
				"x": 1,
				"y": 2,
				"direction": "N"
			},
			"instructions": "LMLMLMLMM"
		},
		{
			"position": {
				"x": 3,
				"y": 3,
				"direction": "E"
			},
			"instructions": "MMRMMRMRRM"
		}
		
	]
}
````

### Output

````
[
    {
        "x": 1,
        "y": 3,
        "direction": "N"
    },
    {
        "x": 5,
        "y": 1,
        "direction": "E"
    }
]
````
